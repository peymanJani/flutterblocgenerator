﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CodeGenerator
{
    public static class FileContents
    {
        public static string BlocFile(string entityName, string[] dto, string[] events)
        {
            string dtoList = "";
            string dtoCopyWith = "";
            string eventLine = "";
            string eventFunctions = "";
            for (int i = 0; i < dto.Length; i++)
            {
                var item = dto[i];
              
               
                dtoList += string.Format("import 'package:estate_app/model/{0}.dart';", ToUnderscoreCase(item)) +  "\r\n";
                dtoCopyWith += string.Format(@" {3}if (event?.action == {0}Actions.get{1}FindAlls) {{
        emit(state.copyWith({2}s: event?.data));
      }}", entityName, item, item.ToLower() , (i > 0 ? "else " : "")) + "\r\n"; ;


            }


            foreach (var item in events)
            {
                eventLine += string.Format("on<{0}{1}Event>(_on{1});", entityName, item) + "\r\n";
                eventFunctions += string.Format(@"FutureOr<void> _on{1}(
                {0}{1}Event event, Emitter<{0}State> emit) {{ }}", entityName, item) + "\r\n";
            }

            string result = string.Format(@"
import 'dart:async';
import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import '../{5}_repository.dart';
import 'package:estate_app/util/base_bloc.dart';
{1}

part '{5}_event.dart';
part '{5}_state.dart';

class {0}Bloc extends BaseBloc<{0}Event, {0}State, {0}Repository> {{
  {0}Bloc(
      {{required {0}Repository repository, required {0}State initialState}})
      : super(repository: repository, initialState: initialState) {{
    repository.status.listen(({0}RepositoryStatus? event) {{
     {2}
    }});

    {3}
  }}

{4}
}}", entityName, dtoList, dtoCopyWith, eventLine , eventFunctions, ToUnderscoreCase(entityName));

            return result;
        }


        public static string BaseViewFile(string entityName, string[] dto, string[] events)
        {
        
         

            string result = string.Format(@"
import '../../../../network/remote_data_source.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import '../../../../helper/Global.dart';
import '../{1}_repository.dart';
import '../bloc/{1}_bloc.dart';
import '{1}_widget.dart';


class {0}Page extends StatelessWidget {{
  

  {0}Page({{Key? key}}) : super(key: key);

  @override
  Widget build(BuildContext context) {{
    return Scaffold(
        backgroundColor: Const.backgroundColor,
        body: BlocProvider(
          create: (_) => {0}Bloc(
              repository: {0}Repository(
                  remoteDataSource: context.read<RemoteDataSource>(),
                  buildContext: context),
              initialState: {0}Initial()),
          child: {0}Widget(),
        ));
  }}
}}
", entityName, ToUnderscoreCase(entityName));

            return result;
        }

        public static string BaseWidgetFile(string entityName, string[] dto)
        {



            string result = string.Format(@"
import 'package:flutter/material.dart';
import '../../../../util/BaseClass.dart';

class {0}Widget extends BaseStateLess {{


  @override
  Widget body(BuildContext context) {{
    return SafeArea(
      child: SingleChildScrollView(
        child: Column(
          children: [

          ],
        ),
      ),
    );
  }}
}}

", entityName, ToUnderscoreCase(entityName));

            return result;
        }

        public static string EventFile(string entityName, string[] events)
        {
            string eventFunctions = "";
            foreach (var item in events)
            {
                eventFunctions += string.Format(@"class {0}{1}Event extends {0}Event{{
  @override
  List<Object?> get props => [];
}}", entityName, item) + "\r\n";
            }
            string result = string.Format(@"
part of '{2}_bloc.dart';

abstract class {0}Event extends Equatable {{
  const {0}Event();
}}

{1}
", entityName, eventFunctions, ToUnderscoreCase(entityName));

            return result;
        }

        public static string BaseFile(string entityName, string[] events)
        {
            string eventFunctions = "";
           
            string result = string.Format(@"export 'bloc/{0}_bloc.dart';
export 'view/view.dart';
export 'model/model.dart';
", ToUnderscoreCase(entityName), eventFunctions);

            return result;
        }


        public static string ViewImportFile(string entityName)
        {
            string eventFunctions = "";

            string result = string.Format(@"export '{0}_page.dart';
export '{0}_widget.dart';", ToUnderscoreCase(entityName), eventFunctions);

            return result;
        }

        public static string StateFile(string entityName,string[] dto, string[] events)
        {
            string dtoList = "";
            string dtoItem = "";
            string constructor = "";
            string props = "";
            string copyItems = "";

            foreach (var item in dto)
            {
                dtoList += string.Format("List<{0}>? {1}s;", item , item.ToLower()) + "\r\n";
                dtoItem += string.Format("{0}? {1};", item , item.ToLower()) + "\r\n";
                constructor += string.Format(@"this.{0}s,this.{0},", item.ToLower()) + "\r\n";
                props += string.Format(@"{0}s,{0},", item.ToLower()) + "\r\n";
                copyItems += string.Format("{1}s: {1}s ?? this.{1}s," + "\r\n" + " {1}: {1} ?? this.{1},", item, item.ToLower()) + "\r\n";
            }
           
            
            string result = string.Format(@"part of '{3}_bloc.dart';

class {0}State extends Equatable {{
  {1}
  {5}

  {0}State(
  {{
  {2}
  }});

  @override
  List<Object?> get props => [
        {4}
      ];

{0}State copyWith({{
    {6}
    {7}    
  }}) {{
    return {0}State(
      {8}
    );
  }}

}}

 
class {0}Initial extends {0}State {{
  {0}Initial() {{
    }}
  }}

", entityName, dtoList,constructor, ToUnderscoreCase(entityName),props,dtoItem, dtoList.Replace(';', ',') , dtoItem.Replace(';', ','), copyItems);

            return result;
        }

        public static string RepositoryFile(string entityName, string[] dtos, string[] events)
        {
            string dtoList = "";
            string getApiList = "";
            string getApiById = "";
            string serverSuccessResponseList = "";
            string serverSuccessResponseDto = "";
            string dtoEnums = "";


         


            foreach (var item in dtos)
            {
                dtoList += string.Format("import '../../../model/{0}.dart';", ToUnderscoreCase(item)) + "\r\n";
                getApiList += string.Format(@"
get{0}s() {{
    remoteData.requestAll(apis.get{0}FindAlls, null, RequestType.GET,
        context: context, loading: false);
}}
get{0}ById() {{
    remoteData.requestAll(apis.get{0}ById, null, RequestType.GET,
        context: context, loading: false);
}}", item) + "\r\n";
                serverSuccessResponseDto += string.Format(@"if (result.str == apis.get{1}ById) {{
         
          add(state?.copyWidth(
              data: {1}.fromJson, action: {0}Actions.get{1}ById));
        }}", entityName, item
     , ToUnderscoreCase(entityName)) + "\r\n";
                serverSuccessResponseList += string.Format(@"if (result.str == apis.get{1}FindAlls) {{
          var dataList = BaseModelResponseList<{1}>.fromJson(
              result.value, {1}.fromJson);
          add(state?.copyWidth(
              data: dataList.data, action: {0}Actions.get{1}FindAlls));
        }}", entityName, item 
       , ToUnderscoreCase(entityName)) + "\r\n";

                dtoEnums += string.Format("get{0}FindAlls,get{0}ById,", item);
            }
        


            string result = string.Format(@"
import 'package:estate_app/network/remote_data_source.dart';
import 'package:estate_app/util/BaseRepository.dart';
import 'package:estate_app/network/Apis.dart' as apis;
import 'package:flutter/material.dart';
import '../../../model/api/BaseResponse.dart';
import '../../../model/api/result.dart';
import '../../../util/request_type.dart';

{1}

class {0}Repository extends BaseRepository<{0}RepositoryStatus> {{
  {0}Repository(
      {{required RemoteDataSource remoteDataSource,
      required BuildContext buildContext}})
      : super(
            state: {0}RepositoryStatus(),
            remoteDataSource: remoteDataSource,
            buildContext: buildContext);

    {2}



  @override
  setServerListener() {{
    remoteData.all().listen((result) {{
      if (result is SuccessState) {{
        {5}
      }} else if (result is SuccessStateNoJson) {{
        {3}
      }}
    }});
  }}
}}

class {0}RepositoryStatus implements BaseRepositoryStatus<{0}Actions> {{
  @override
  {0}Actions? action;

  dynamic data;

  {0}RepositoryStatus({{this.action, this.data}});

  {0}RepositoryStatus copyWidth({{{0}Actions? action, dynamic data}}) {{
    return {0}RepositoryStatus(
        action: action ?? this.action, data: data ?? this.data);
  }}
}}

enum {0}Actions {{
  {4}
}}

", entityName, dtoList, getApiList , serverSuccessResponseList , dtoEnums  , serverSuccessResponseDto);

            return result;
        }
        public static string ToUnderscoreCase(string str)
    => string.Concat((str ?? string.Empty).Select((x, i) => i > 0 && i < str.Length - 1 && char.IsUpper(x) && !char.IsUpper(str[i - 1]) ? $"_{x}" : x.ToString())).ToLower();


    }
}
