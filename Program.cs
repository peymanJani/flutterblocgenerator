﻿// See https://aka.ms/new-console-template for more information
Console.Write("Welcome To The Code Generator :");
string[] readLine = Console.ReadLine().Split(' ');
string className = readLine[0];
string[] dtos = readLine[1].Split(',');
string[] events = readLine[2].Split(',');
//string[] dto = readLine[3].Split(',');

string fileName = "";
string text = "";
string snacCaseClassName = ToUnderscoreCase(className);
string blocDirectoryName = snacCaseClassName+ "/bloc";
string viewDirectoryName = snacCaseClassName+ "/view";
string modelDirectoryName = snacCaseClassName + "/model";

createDirectory(blocDirectoryName);
createDirectory(viewDirectoryName);
createDirectory(modelDirectoryName);



#region bloc
fileName = blocDirectoryName + "/" + snacCaseClassName + "_bloc.dart";
text = CodeGenerator.FileContents.BlocFile(className, dtos, events);
CreateFile(fileName, text);
#endregion


#region viewImport
fileName = viewDirectoryName + "/" + "view.dart";
text = CodeGenerator.FileContents.ViewImportFile(className);
CreateFile(fileName, text);
#endregion

#region modelImport
fileName = modelDirectoryName + "/" + "model.dart";
text = "";
CreateFile(fileName, text);
#endregion

#region basePage
fileName = viewDirectoryName + "/" + snacCaseClassName + "_page.dart";
text = CodeGenerator.FileContents.BaseViewFile(className, dtos, events);
CreateFile(fileName, text);
#endregion


#region baseWidget
fileName = viewDirectoryName + "/" + snacCaseClassName + "_widget.dart";
text = CodeGenerator.FileContents.BaseWidgetFile(className, dtos);
CreateFile(fileName, text);
#endregion


#region base
fileName = snacCaseClassName + "/" + snacCaseClassName + ".dart";
text = CodeGenerator.FileContents.BaseFile(className, dtos);
CreateFile(fileName, text);
#endregion

#region event
fileName = blocDirectoryName + "/" + snacCaseClassName + "_event.dart";
text = CodeGenerator.FileContents.EventFile(className, events);
CreateFile(fileName, text);
#endregion

#region state
fileName = blocDirectoryName + "/" + snacCaseClassName + "_state.dart";
text = CodeGenerator.FileContents.StateFile(className, dtos, events);
CreateFile(fileName, text);
#endregion


#region repository
fileName =  snacCaseClassName + "/" + snacCaseClassName + "_repository.dart";
text = CodeGenerator.FileContents.RepositoryFile(className, dtos, events);
CreateFile(fileName, text);
Console.Write("All Codes Generated");
Console.ReadLine();
#endregion





void CreateFile(string path, string text)
{
    using (var tw = new StreamWriter(path, false))
    {
        tw.WriteLine(text);
    }

}

void CopyFile(string src, string dest)
{
    if (File.Exists(src))
        if (!File.Exists(dest))
            File.Copy(src, dest);
}
 string ToUnderscoreCase( string str)
    => string.Concat((str ?? string.Empty).Select((x, i) => i > 0 && i < str.Length - 1 && char.IsUpper(x) && !char.IsUpper(str[i - 1]) ? $"_{x}" : x.ToString())).ToLower();

 void createDirectory(string blocDirectoryName)
{

    bool blocDirectoryExist = System.IO.Directory.Exists(blocDirectoryName);
    if (!blocDirectoryExist)
    {
        System.IO.Directory.CreateDirectory(blocDirectoryName);
    }

}
