
import 'package:estate_app/network/remote_data_source.dart';
import 'package:estate_app/util/BaseRepository.dart';
import 'package:estate_app/network/Apis.dart' as apis;
import 'package:flutter/material.dart';
import '../../../model/api/BaseResponse.dart';
import '../../../model/api/result.dart';
import '../../../util/request_type.dart';

import '../../../model/lawn_lease.dart';


class LawnLeaseRepository extends BaseRepository<LawnLeaseRepositoryStatus> {
  LawnLeaseRepository(
      {required RemoteDataSource remoteDataSource,
      required BuildContext buildContext})
      : super(
            state: LawnLeaseRepositoryStatus(),
            remoteDataSource: remoteDataSource,
            buildContext: buildContext);

    
getLawnLeases() {
    remoteData.requestAll(apis.getLawnLeaseFindAlls, null, RequestType.GET,
        context: context, loading: false);
}
getLawnLeaseById() {
    remoteData.requestAll(apis.getLawnLeaseById, null, RequestType.GET,
        context: context, loading: false);
}




  @override
  setServerListener() {
    remoteData.all().listen((result) {
      if (result is SuccessState) {
        if (result.str == apis.getLawnLeaseById) {
         
          add(state?.copyWidth(
              data: LawnLease.fromJson, action: LawnLeaseActions.getLawnLeaseById));
        }

      } else if (result is SuccessStateNoJson) {
        if (result.str == apis.getLawnLeaseFindAlls) {
          var dataList = BaseModelResponseList<LawnLease>.fromJson(
              result.value, LawnLease.fromJson);
          add(state?.copyWidth(
              data: dataList.data, action: LawnLeaseActions.getLawnLeaseFindAlls));
        }

      }
    });
  }
}

class LawnLeaseRepositoryStatus implements BaseRepositoryStatus<LawnLeaseActions> {
  @override
  LawnLeaseActions? action;

  dynamic data;

  LawnLeaseRepositoryStatus({this.action, this.data});

  LawnLeaseRepositoryStatus copyWidth({LawnLeaseActions? action, dynamic data}) {
    return LawnLeaseRepositoryStatus(
        action: action ?? this.action, data: data ?? this.data);
  }
}

enum LawnLeaseActions {
  getLawnLeaseFindAlls,getLawnLeaseById,
}


