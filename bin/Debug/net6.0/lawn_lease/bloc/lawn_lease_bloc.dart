
import 'dart:async';
import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import '../lawn_lease_repository.dart';
import 'package:estate_app/util/base_bloc.dart';
import 'package:estate_app/model/lawn_lease.dart';


part 'lawn_lease_event.dart';
part 'lawn_lease_state.dart';

class LawnLeaseBloc extends BaseBloc<LawnLeaseEvent, LawnLeaseState, LawnLeaseRepository> {
  LawnLeaseBloc(
      {required LawnLeaseRepository repository, required LawnLeaseState initialState})
      : super(repository: repository, initialState: initialState) {
    repository.status.listen((LawnLeaseRepositoryStatus? event) {
      if (event?.action == LawnLeaseActions.getLawnLeaseFindAlls) {
        emit(state.copyWith(lawnleases: event?.data));
      }

    });

    on<LawnLeaseShowDetailsEvent>(_onShowDetails);
on<LawnLeaseUseLawnLeaseEvent>(_onUseLawnLease);

  }

FutureOr<void> _onShowDetails(
                LawnLeaseShowDetailsEvent event, Emitter<LawnLeaseState> emit) { }
FutureOr<void> _onUseLawnLease(
                LawnLeaseUseLawnLeaseEvent event, Emitter<LawnLeaseState> emit) { }

}
