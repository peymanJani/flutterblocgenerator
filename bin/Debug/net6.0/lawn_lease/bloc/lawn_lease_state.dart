part of 'lawn_lease_bloc.dart';

class LawnLeaseState extends Equatable {
  List<LawnLease>? lawnleases;

  LawnLease? lawnlease;


  LawnLeaseState(
  {
  this.lawnleases,this.lawnlease,

  });

  @override
  List<Object?> get props => [
        lawnleases,lawnlease,

      ];

LawnLeaseState copyWith({
    List<LawnLease>? lawnleases,

    LawnLease? lawnlease,
    
  }) {
    return LawnLeaseState(
      lawnleases: lawnleases ?? this.lawnleases,
 lawnlease: lawnlease ?? this.lawnlease,

    );
  }

}

 
class LawnLeaseInitial extends LawnLeaseState {
  LawnLeaseInitial() {
    }
  }


