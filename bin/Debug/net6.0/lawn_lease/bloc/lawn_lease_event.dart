
part of 'lawn_lease_bloc.dart';

abstract class LawnLeaseEvent extends Equatable {
  const LawnLeaseEvent();
}

class LawnLeaseShowDetailsEvent extends LawnLeaseEvent{
  @override
  List<Object?> get props => [];
}
class LawnLeaseUseLawnLeaseEvent extends LawnLeaseEvent{
  @override
  List<Object?> get props => [];
}


