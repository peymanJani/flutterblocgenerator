
import '../../../../network/remote_data_source.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import '../../../../helper/Global.dart';
import '../lawn_lease_repository.dart';
import '../bloc/lawn_lease_bloc.dart';
import 'lawn_lease_widget.dart';


class LawnLeasePage extends StatelessWidget {
  

  LawnLeasePage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: Const.backgroundColor,
        body: BlocProvider(
          create: (_) => LawnLeaseBloc(
              repository: LawnLeaseRepository(
                  remoteDataSource: context.read<RemoteDataSource>(),
                  buildContext: context),
              initialState: LawnLeaseInitial()),
          child: LawnLeaseWidget(),
        ));
  }
}

