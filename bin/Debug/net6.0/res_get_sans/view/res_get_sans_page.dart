
import '../../../../network/remote_data_source.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import '../../../../helper/Global.dart';
import '../res_get_sans_repository.dart';
import '../bloc/res_get_sans_bloc.dart';
import 'res_get_sans_widget.dart';


class ResGetSansPage extends StatelessWidget {
  

  ResGetSansPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: Const.backgroundColor,
        body: BlocProvider(
          create: (_) => ResGetSansBloc(
              repository: ResGetSansRepository(
                  remoteDataSource: context.read<RemoteDataSource>(),
                  buildContext: context),
              initialState: ResGetSansInitial()),
          child: ResGetSansWidget(),
        ));
  }
}

