
import 'dart:async';
import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import '../res_get_sans_repository.dart';
import 'package:estate_app/util/base_bloc.dart';
import 'package:estate_app/model/transaction.dart';


part 'res_get_sans_event.dart';
part 'res_get_sans_state.dart';

class ResGetSansBloc extends BaseBloc<ResGetSansEvent, ResGetSansState, ResGetSansRepository> {
  ResGetSansBloc(
      {required ResGetSansRepository repository, required ResGetSansState initialState})
      : super(repository: repository, initialState: initialState) {
    repository.status.listen((ResGetSansRepositoryStatus? event) {
      if (event?.action == ResGetSansActions.getTransactionFindAlls) {
        emit(state.copyWith(transactions: event?.data));
      }

    });

    on<ResGetSansReastartAppEvent>(_onReastartApp);

  }

FutureOr<void> _onReastartApp(
                ResGetSansReastartAppEvent event, Emitter<ResGetSansState> emit) { }

}
