part of 'res_get_sans_bloc.dart';

class ResGetSansState extends Equatable {
  List<Transaction>? transactions;

  Transaction? transaction;


  ResGetSansState(
  {
  this.transactions,this.transaction,

  });

  @override
  List<Object?> get props => [
        transactions,transaction,

      ];

ResGetSansState copyWith({
    List<Transaction>? transactions,

    Transaction? transaction,
    
  }) {
    return ResGetSansState(
      transactions: transactions ?? this.transactions,
 transaction: transaction ?? this.transaction,

    );
  }

}

 
class ResGetSansInitial extends ResGetSansState {
  ResGetSansInitial() {
    }
  }


