
import 'package:estate_app/network/remote_data_source.dart';
import 'package:estate_app/util/BaseRepository.dart';
import 'package:estate_app/network/Apis.dart' as apis;
import 'package:flutter/material.dart';
import '../../../model/api/BaseResponse.dart';
import '../../../model/api/result.dart';
import '../../../util/request_type.dart';

import '../../../model/transaction.dart';


class ResGetSansRepository extends BaseRepository<ResGetSansRepositoryStatus> {
  ResGetSansRepository(
      {required RemoteDataSource remoteDataSource,
      required BuildContext buildContext})
      : super(
            state: ResGetSansRepositoryStatus(),
            remoteDataSource: remoteDataSource,
            buildContext: buildContext);

    
getTransactions() {
    remoteData.requestAll(apis.getTransactionFindAlls, null, RequestType.GET,
        context: context, loading: false);
}
getTransactionById() {
    remoteData.requestAll(apis.getTransactionById, null, RequestType.GET,
        context: context, loading: false);
}




  @override
  setServerListener() {
    remoteData.all().listen((result) {
      if (result is SuccessState) {
        if (result.str == apis.getTransactionById) {
         
          add(state?.copyWidth(
              data: Transaction.fromJson, action: ResGetSansActions.getTransactionById));
        }

      } else if (result is SuccessStateNoJson) {
        if (result.str == apis.getTransactionFindAlls) {
          var dataList = BaseModelResponseList<Transaction>.fromJson(
              result.value, Transaction.fromJson);
          add(state?.copyWidth(
              data: dataList.data, action: ResGetSansActions.getTransactionFindAlls));
        }

      }
    });
  }
}

class ResGetSansRepositoryStatus implements BaseRepositoryStatus<ResGetSansActions> {
  @override
  ResGetSansActions? action;

  dynamic data;

  ResGetSansRepositoryStatus({this.action, this.data});

  ResGetSansRepositoryStatus copyWidth({ResGetSansActions? action, dynamic data}) {
    return ResGetSansRepositoryStatus(
        action: action ?? this.action, data: data ?? this.data);
  }
}

enum ResGetSansActions {
  getTransactionFindAlls,getTransactionById,
}


